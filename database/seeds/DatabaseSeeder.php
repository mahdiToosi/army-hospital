<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Oprator::class, 1)->create();

        factory(App\Specialist::class, 2)->create();

        factory(App\Doctor::class, 4)->create();

        factory(App\WorkTime::class, 20)->create();

        factory(App\Patient::class, 50)->create();
        
        factory(App\Off::class, 10)->create();
    }
}
