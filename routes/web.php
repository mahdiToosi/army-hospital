<?php

Route::get('/', 'OpratorAuth\LoginController@showLoginForm')->name('login');
Route::get('/get/Doctors/today', 'OpratorController@getDoctors');

Route::group(['prefix' => 'oprator'], function () {
      Route::post('/login', 'OpratorAuth\LoginController@login');
      Route::post('/logout', 'OpratorAuth\LoginController@logout')->name('logout');
      Route::post('/delete/{id}', 'OpratorController@delete')->name('Oprator.delete');
      Route::get('/home', 'OpratorController@showHome')->name('Oprator.Home');

      Route::get('/register', 'OpratorAuth\RegisterController@showRegistrationForm')->name('oprator.register');
      Route::post('/register', 'OpratorAuth\RegisterController@register');
});

Route::group(['prefix' => 'admin'], function () {
      Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('AdminLogin');
      Route::post('/login', 'AdminAuth\LoginController@login');
      Route::post('/logout', 'AdminAuth\LoginController@logout')->name('AdminLogout');

      Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('admin.register');
      Route::post('/register', 'AdminAuth\RegisterController@register');
});

Route::resource('doctor', 'DoctorController');
Route::get('/get/Doctors', 'DoctorController@getDoctors');
Route::get('/get/Doctors/withSpecialityId/{id}', 'DoctorController@getDoctorsWithSpecialityId');

Route::resource('specialist', 'SpecialistController');
Route::get('/get/Specialitys', 'SpecialistController@getSpecialitys');

Route::resource('off', 'OffController');

Route::resource('patient', 'PatientController')->except('destroy');
Route::post('/patient/{id}', 'PatientController@destroy');
Route::get('/get/patients/{date}/{docId}', 'PatientController@getPatientWithDate');
Route::get('/patient/done/{id}', 'PatientController@patientDone');

Route::resource('work-time', 'WorkTimeController');