require('./bootstrap');
window.Vue = require('vue');

Vue.component('lists', require('./components/lists.vue'));
Vue.component('slick', require('vue-slick'));

import Vuebar from 'vuebar';
import {store} from './store/store';

Vue.use(Vuebar);
export const eventBus = new Vue();

const app = new Vue({
      el: '#app',
      store ,
      data: {
            time: '',
            date: '',
            slickOptions: {
                  arrows: false,
                  touchMove: false,
                  pauseOnFocus: false,
                  pauseOnHover: false,
                  draggable: false,
                  fade: true,
                  autoplay:true,
                  autoplaySpeed: 15000,
                  zIndex: 1,
                  rtl: true,
            },
            specialistListShow: true,
            todayWeDontHaveDoc: false
      },
      methods:{
            clickedOnAddInDesk(){
                  eventBus.$emit('openModal')
            }
      },
      created(){
            eventBus.$on('todayWeDontHaveDoc', () => {
                  this.todayWeDontHaveDoc = true
            })
            eventBus.$on('todayWeHaveDoc', () => {
                  this.todayWeDontHaveDoc = false
            })
      }
});







// clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk

updateTime();
function updateTime() {
      let cd = new Date();
      app.time = zeroPadding(cd.getHours(), 2) + ':' + zeroPadding(cd.getMinutes(), 2) ;
      app.date = new persianDate().format("dddd") + ' ' + new persianDate().format("L");
};

function zeroPadding(num, digit) {
      let zero = '';
      for(let i = 0; i < digit; i++) {
            zero += '0';
      }
      return (zero + num).slice(-digit);
}
// clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk clockkkkk
