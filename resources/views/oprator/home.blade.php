@extends('layouts.app')

@section('content')
      {{--log out button --}}
      <!-- OPRATOR PAGE -->
      <!-- clock -->
      <div class="upOnit">
            <div class="row large-20" id="opratorPage">

                  <div id="clock" class="row large-20 align-center">
                        <div class="column large-19  ">
                              <div class="row align-center">
                                    <div class="time " v-text="time"></div>
                              </div>
                              <div class="row align-center">
                                    <div class=" date" v-text="date" ></div>
                              </div>
                        </div>
                  </div>
                  <!-- endclock -->

                  <!-- DOCTORS LIST -->
                  <div class="row large-20">
                        <div class="listSpecialist" v-bar="{
                                                      preventParentScroll: true,
                                                      scrollThrottle: 1,
                                                }"
                              v-show="specialistListShow">
                              <div>
                                    <div class="row align-center-middle large-20" >
                                          <div class="column large-17 ">

                                                <!-- *********************** -->

                                                <lists></lists>

                                                <!-- *********************** -->
                                          </div>
                                    </div>
                              </div>

                        </div>

                        <a @click="specialistListShow = true"
                              v-if="!todayWeDontHaveDoc"
                              v-show="!specialistListShow "
                              role="button" class="rightBs">
                              
                              <img src="{{asset('img/logo.png')}}" width="50px" alt="show list">
                        
                        </a>

                        <a @click="specialistListShow = false"
                              v-if="!todayWeDontHaveDoc"
                              v-show="specialistListShow"
                              role="button" class="rightBs">
                                    
                              <img src="{{asset('img/close.svg')}}" width="50px"
                                    style="border-radius: 50%;" alt="don't show list">
                        </a>

                        <a @click="clickedOnAddInDesk()"
                              v-if="todayWeDontHaveDoc"
                              role="button" class="rightBs"
                              style=" opacity: 0.5;">
                              
                        <img src="{{asset('img/add.svg')}}" width="50px"
                              style="border-radius: 50%;" alt="don't show list">
                        
                        </a>

                        <a role="button" onclick="
                              event.preventDefault();
                              document.getElementById('logout-form').submit();
                              "
                              class="logoutB">

                              <img src="{{asset('img/logout.svg')}}" width="45px"
                              style="border-radius: 50%; opacity: 0.5" alt="خارج شدن">
                        
                        </a>

                        <form id="logout-form" action="{{ url('/oprator/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                        </form>

                  </div>
            </div>
      </div>

      <!-- END DOCTORS LIST -->
      <!-- END OPRATOR PAGE -->
@endsection
