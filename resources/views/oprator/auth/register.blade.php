@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <br>
            <br>
            <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                              <div class="panel panel-default">
                                    <div class="panel-heading text-right">اپراتور جدید</div>
                                    <div class="panel-body">
                                          <form class="form-horizontal" role="form" method="POST" action="{{ url('/oprator/register') }}">
                                                {{ csrf_field() }}

                                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                      <label for="username" class="col-md-4 control-label">Username</label>

                                                      <div class="col-md-6">
                                                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus>

                                                            @if ($errors->has('username'))
                                                                  <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                                            @endif
                                                      </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                      <label for="password" class="col-md-4 control-label">Password</label>

                                                      <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control" name="password">

                                                            @if ($errors->has('password'))
                                                                  <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                            @endif
                                                      </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                      <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                      <div class="col-md-6">
                                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                                            @if ($errors->has('password_confirmation'))
                                                                  <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                                            @endif
                                                      </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                      <div class="col-md-6 col-md-offset-6">
                                                            <button type="submit" class="btn btn-success">
                                                                  Register
                                                            </button>
                                                      </div>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                        <br>
                        <div class="row">
                              <div class="col-md-8 col-md-offset-2">

                                    <table class="table table-borderless">
                                          <thead>
                                          <tr>
                                                <th class="text-center">Username</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          @foreach($oprators as $oprator)
                                                <tr>
                                                      <td class="text-center">{{ $oprator->username }}</td>
                                                      <td class="text-center">
                                                            {!! Form::open([
                                                                'method'=>'POST',
                                                                'url' => ['oprator/delete', $oprator->id],
                                                                'style' => 'display:inline'
                                                            ]) !!}
                                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> حذف', array(
                                                                    'type' => 'submit',
                                                                    'class' => 'btn btn-danger btn-sm',
                                                                    'title' => 'Delete Off',
                                                                    'onclick'=>'return confirm("Confirm delete?")'
                                                            )) !!}
                                                            {!! Form::close() !!}
                                                      </td>
                                                </tr>
                                          @endforeach
                                          </tbody>
                                    </table>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
