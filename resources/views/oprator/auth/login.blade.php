@extends('layouts.app')

@section('style')
    <style>
        .slickcss {
            /* filter : blur(1px); */
        }
    </style>
@endsection

@section('content')

    <!-- LOGIN PAGE -->

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/oprator/login') }}">
        {{ csrf_field() }}
        <div id="login" class="row align-center">
            <div class="column"></div>
            <div class="column">
                <div class="row align-center" style="margin-bottom: 15px; width: 280px; height: 280px;">
                    <img class="" src="{{asset('img/armyLogo.png')}}" id="logo" alt="">
                </div>
                <div class="row align-center">
                    <div class="column large-18" >
                        <input type="text" class="login"
                                style="direction: ltr" name="username"
                                placeholder="نام کاربری"
                                value="{{ old('username') }}" autofocus>
                        <input  type="password" class="login "
                                style="direction: ltr" name="password"
                                placeholder="رمز عبور">

                        <button type="submit">
                            <img src="{{asset("img/check.png")}}" alt="ورود" style="width: 17%">
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </form>

    <!-- END LOGIN PAGE -->

@endsection
