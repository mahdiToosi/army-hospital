@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <div class="row">

                  <div class="col-md-9">
                        <div class="card">
                              <div class="card-header">WorkTime {{ $worktime->id }}</div>
                              <div class="card-body">

                                    <a href="{{ url('/work-time') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    <a href="{{ url('/work-time/' . $worktime->id . '/edit') }}" title="Edit WorkTime"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['worktime', $worktime->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Delete WorkTime',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    ))!!}
                                    {!! Form::close() !!}
                                    <br/>
                                    <br/>

                                    <div class="table-responsive">
                                          <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                      <th>نام دکتر</th>
                                                      <td> {{ $worktime->doctor->name }} </td>
                                                </tr>
                                                <tr>
                                                      <th>روز هفته</th>
                                                      <td> {{ $worktime->day}} </td>
                                                </tr>
                                                <tr>
                                                      <th> شروع زمان کاری </th>
                                                      <td> {{ $worktime->start }} </td>
                                                </tr>
                                                <tr>
                                                      <th> پایان زمان کاری </th>
                                                      <td> {{ $worktime->end }} </td>
                                                </tr>
                                                </tbody>
                                          </table>
                                    </div>

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
