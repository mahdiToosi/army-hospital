<br>
<br>
<div class="row mgb22">
      <div class="col-md-4 col-lg-offset-2">
            <div class="row">
                  <div class="col-md-4">
                        {!! Form::number('visit_time', 10, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                        {!! $errors->first('visit_time', '<p class="help-block">:message</p>') !!}
                  </div>
                  <div class="col-md-7">
                        {!! Form::label('visit_time', 'زمان تقریبی هر ویزیت به دقیقه', ['class' => ' control-label']) !!}
                  </div>
            </div>
      </div>
      <div class="col-md-5">
            <div class="row">
                  <div class="col-md-6">
                        <select name="dr_id" class="form-control">
                              @foreach($docs as $doc)
                                    <option value="{{$doc->id}}">{{$doc->name}}</option>
                              @endforeach
                        </select>
                  </div>
                  <div class="col-md-6">
                        {!! Form::label('dr_id', 'برای دکتر', ['class' => ' control-label ']) !!}
                  </div>
            </div>
      </div>
</div>

<div class="row mgb22">
      <div class="col-md-4 col-lg-offset-2">
            <div class="row">
                  <div class="col-md-6" style="margin-left: 26px;">
                        <date-picker
                              v-model="createEndTime"
                              type="time"
                              format="HH:mm:00"
                              display-format="HH : mm"
                        >
                        </date-picker>
                        <input type="hidden" v-model="createEndTime" name="end" value="">
                  </div>
                  <div class="col-md-4">
                        {!! Form::label('end', 'پایان زمان کاری', ['class' => ' control-label']) !!}
                  </div>
            </div>
      </div>
      <div class="col-md-4 " style="margin-left: 13px;">
            <div class="row">
                  <div class="col-md-6" style="margin-left: 36px;">
                        <date-picker
                              v-model="createStartTime"
                              type="time"
                              format="HH:mm:00"
                              display-format="HH : mm"
                        >
                        </date-picker>
                        <input type="hidden" v-model="createStartTime" name="start" value="">
                  </div>
                  <div class="col-md-4">
                        {!! Form::label('start', 'شروع زمان کاری', ['class' => 'control-label']) !!}
                  </div>
            </div>
      </div>
</div>

<div class="row mgb22">
      <div class="col-md-3 col-lg-offset-3" style="padding-left: 150px;">
            {!! Form::submit('ثبت   ', ['class' => 'btn btn-success btn-default' , 'style' =>
                                                                                                                'padding-right: 25px;
                                                                                                                padding-left: 25px;
                                                                                                                padding-top: 10px;
                                                                                                                padding-bottom: 10px;']) !!}
      </div>
      <div class="col-md-2 "style="margin-left: 52px;">
            <select name="day_id" class="form-control">
                  <option value="" disabled selected hidden>روز هفته</option>
                  @foreach($days as $key => $day)
                        <option value="{{$key}}">{{$day}}</option>
                  @endforeach
            </select>
      </div>
      <div class="col-md-3">
            {!! Form::label('day_id', 'روز هفته', ['class' => 'control-label']) !!}
      </div>

</div>
