@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <div class="row">
                  {!! Form::open(['url' => '/work-time', 'class' => 'form-horizontal', 'files' => true]) !!}
                  @include ('work-time.form')
                  {!! Form::close() !!}

            </div>
            <div class="row">
                  <br>
                  <div class="col-md-12">
                        <div class="card">
                              <div class="card-body ">
                                    <br>
                                    <div class="table-responsive">
                                          <table class="table table-borderless">
                                                <thead><tr>
                                                      <th class="text-center">#</th>
                                                      <th class="text-center">نام دکتر</th>
                                                      <th class="text-center">روز هفته</th>
                                                      <th class="text-center">شروع زمان کاری</th>
                                                      <th class="text-center">پایان زمان کاری</th>
                                                </tr></thead>

                                                <tbody>
                                                @foreach($worktime as $item)
                                                      <tr>
                                                            <td class="text-center">{{ $loop->iteration or $item->id }}</td>
                                                            <td class="text-center">{{ $item->doctor->name }}</td>
                                                            <td class="text-center">{{ $item->day }}</td>
                                                            <td class="text-center">{{ $item->start }}</td>
                                                            <td class="text-center">{{ $item->end }}</td>
                                                            <td class="text-center">
                                                                  {!! Form::open([
                                                                      'method'=>'DELETE',
                                                                      'url' => ['/work-time', $item->id],
                                                                      'style' => 'display:inline'
                                                                  ]) !!}
                                                                  {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> حذف', array(
                                                                          'type' => 'submit',
                                                                          'class' => 'btn btn-danger btn-sm',
                                                                          'title' => 'Delete WorkTime',
                                                                          'onclick'=>'return confirm("Confirm delete?")'
                                                                  )) !!}
                                                                  {!! Form::close() !!}
                                                            </td>
                                                      </tr>
                                                @endforeach
                                                </tbody>
                                          </table>
                                          <div class="pagination-wrapper"> {!! $worktime->appends(['search' => Request::get('search')])->render() !!} </div>
                                    </div>

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
