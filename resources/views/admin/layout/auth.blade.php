<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel Multi Auth Guard') }}</title>

      <!-- Styles -->
      <link rel="stylesheet" href="{{asset('css/admin.css')}}">

      <!-- Scripts -->
      <script>
            window.Laravel = <?php echo json_encode([
                  'csrfToken' => csrf_token(),
            ]); ?>
      </script>
</head>
<body>
@if(\Request::route()->getname() === 'register')
      <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                  <div class="navbar-header">
                        <!-- Branding Image -->
                        <a class="navbar-brand" role="button">
                              {{ config('app.name', 'Laravel Multi Auth Guard') }}: Oprator
                        </a>
                  </div>
            </div>
      </nav>
@else
      <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                  <div class="navbar-header">
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/admin') }}">
                              {{ config('app.name', 'Laravel Multi Auth Guard') }}: Admin
                        </a>
                  </div>
                  <ul class="nav navbar-nav">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li class="{{\Request::route()->getname() === 'doctor.index' ? 'active' : ''}}"><a href="{{route('doctor.index')}}">دکتر ها</a></li>
                        <li class="{{\Request::route()->getname() === 'work-time.index' ? 'active' : ''}}"><a href="{{route('work-time.index')}}">برنامه کاری</a></li>
                        <li class="{{\Request::route()->getname() === 'off.index' ? 'active' : ''}}"><a href="{{route('off.index')}}">مرخصی</a></li>
                        <li class="{{\Request::route()->getname() === 'oprator.register' ? 'active' : ''}}"><a href="{{route('oprator.register')}}">اپراتور جدید</a></li>
                  </ul>

                  <ul class="navbar-right nav navbar-nav">
                        <li>
                              <a href="{{ url('/admin/logout') }}"
                                 role="button"
                                 onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                                    خارج شدن
                              </a>
                        </li>

                        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                        </form>
                  </ul>
            </div>
      </nav>
@endif
<div id="admin">
      @yield('content')
</div>

<!-- Scripts -->
<script src="{{asset('js/persian-date.js')}}" ></script>
<script src="{{asset('js/bootstrap.js')}}" ></script>
<script src="{{asset('js/admin.js')}}"></script>
@yield('js')
</body>
</html>
