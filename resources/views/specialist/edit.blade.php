@extends('admin.layout.auth')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Specialist #{{ $specialist->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/specialist') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($specialist, [
                            'method' => 'PATCH',
                            'url' => ['/specialist', $specialist->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('specialist.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
