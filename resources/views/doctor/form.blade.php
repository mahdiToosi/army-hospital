<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
      <div class="col-md-6">
            {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control text-right', 'required' => 'required']
                                                                              : ['class' => 'form-control text-right']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
      {!! Form::label('name', 'نام و نام خانوادگی', ['class' => 'control-label']) !!}

</div>
<div class="form-group {{ $errors->has('phone_number') ? 'has-error' : ''}}">
      <div class="col-md-6">
            {!! Form::number('phone_number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
      </div>
      {!! Form::label('phone_number', 'شماره تلفن', ['class' => 'control-label']) !!}

</div>
<div class="form-group {{ $errors->has('specialist_id') ? 'has-error' : ''}}">
      <div class="col-md-6">
            <select name="specialist_id" class="form-control">
                  @foreach($specialitys as $speciality)
                        <option value="{{$speciality->id}}">{{$speciality->speciality}}</option>
                  @endforeach
            </select>
      </div>
      {!! Form::label('speciality', 'متخصص', ['class' => ' control-label']) !!}
</div>
<br>
<div class="form-group">
      <div class="col-md-offset-2 col-md-4">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
      </div>
</div>
