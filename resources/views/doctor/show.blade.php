@extends('admin.layout.auth')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Doctor {{ $doctor->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/doctor') }}" title="OK"><button class="btn btn-success btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>صحیح</button></a>
                        <a href="{{ url('/doctor/' . $doctor->id . '/edit') }}" title="Edit Doctor"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['doctor', $doctor->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete Doctor',
                                'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                {{--<tr>--}}
                                    {{--<th>ID</th><td>{{ $doctor->id }}</td>--}}
                                {{--</tr>--}}
                                <tr><th>نام و نام خانوادگی</th><td> {{ $doctor->name }} </td></tr>
                                <tr><th>شماره تلفن</th><td> {{ $doctor->phone_number }} </td></tr>
                                <tr><th>متخصص</th><td> {{ $doctor->speciality->speciality }} </td></tr>
                                @foreach($doctor->workTime as $work)
                                    <tr>
                                        <th> روز کاری </th>
                                        <td> {{ $work->day }} </td>
                                        <td> {{ $work->start }} </td>
                                        <td> {{ $work->end }} </td>
                                        <td></td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
