@extends('admin.layout.auth')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-8" id="creaeteDoc">
                    <br />
                    <br />
                <div class="card">
                    <div class="card-header text-center">اضافه کردن دکتر جدید</div>
                    <div class="card-body">
                            <br />
                            <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/doctor', 'class' => 'form-horizontal', 'files' => true]) !!}
                        <br>
                        @include ('doctor.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            <div class="col-md-3"> </div>
        </div>
    </div>
@endsection
@section('js')
    <script></script>
@endsection
