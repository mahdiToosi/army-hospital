@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <br>
            <br>
            <br>
            <div class="row">
                  <div class="col-md-12">
                        <div class="card">
                              <div class="card-body ">
                                    <div class="row col-lg-offset-2">
                                          <div class="col-lg-2">
                                                <a href="{{ route('specialist.index') }}" class="btn btn-primary btn-sm" title="Add New Doctor">
                                                      <i class="fa fa-plus" aria-hidden="true"></i> اضافه کردن تخصص جدید
                                                </a>
                                          </div>
                                          <div class="col-lg-2">
                                                <a href="{{ route('doctor.create') }}" class="btn btn-success btn-sm" title="Add New Doctor">
                                                      <i class="fa fa-plus" aria-hidden="true"></i> اضافه کردن دکتر جدید
                                                </a>
                                          </div>
                                          <div class="col-md-4">
                                                {!! Form::open(['method' => 'GET', 'url' => '/doctor', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                                <span class="input-group-append">
                                                    <button class="btn btn-secondary" type="submit">
                                                            <i class="fa fa-search"></i>                                    جستجو کن
                                                    </button>
                                                </span>
                                                {!! Form::close() !!}
                                          </div>
                                    </div>

                                    <br/>
                                    <br/>
                                    <div class="table-responsive">
                                          <table class="table table-borderless text-center">
                                                <thead>
                                                <tr>
                                                      <th class="text-center">#</th>
                                                      <th class="text-center">نام و نام خانوادگی</th>
                                                      <th class="text-center">روز های کاری</th>
                                                      <th class="text-center">شماره تلفن</th>
                                                      <th class="text-center">متخصص</th>
                                                      <th class="text-center">گزینه ها</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($doctor as $item)
                                                      <tr>
                                                            <td>{{ $loop->iteration or $item->id }}</td>
                                                            <td>{{ $item->name }}</td>
                                                            <td>@foreach($item->workTime as $time) {{$time->day .','}} @endforeach</td>
                                                            <td>{{ $item->phone_number }}</td>
                                                            <td>{{ $item->speciality->speciality }}</td>
                                                            <td>
                                                                  <a href="{{ url('/doctor/' . $item->id . '/edit') }}" title="Edit Doctor"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> تغییر</button></a>
                                                                  {!! Form::open([
                                                                      'method'=>'DELETE',
                                                                      'url' => ['/doctor', $item->id],
                                                                      'style' => 'display:inline'
                                                                  ]) !!}
                                                                  {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> حذف', array(
                                                                          'type' => 'submit',
                                                                          'class' => 'btn btn-danger btn-sm',
                                                                          'title' => 'Delete Doctor',
                                                                          'onclick'=>'return confirm("Confirm delete?")'
                                                                  )) !!}
                                                                  {!! Form::close() !!}
                                                            </td>
                                                      </tr>
                                                @endforeach
                                                </tbody>
                                          </table>
                                          <div class="pagination-wrapper"> {!! $doctor->appends(['search' => Request::get('search')])->render() !!} </div>
                                    </div>

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
