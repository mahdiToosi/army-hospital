@extends('admin.layout.auth')

@section('content')
      <div class="container">
            <div class="row">

                  <div class="col-md-12">
                        <div class="card">
                              <div class="card-header col-lg-offset-5 ">
                                    <h2>
                                          {{ $doctor->name }} #
                                    </h2>
                              </div>
                              <div class="card-body col-lg-offset-4">
                                    <a href="{{ url('/doctor') }}" title="Back">
                                          <button class="btn btn-warning btn-sm">
                                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                          </button>
                                    </a>
                                    <br />
                                    <br />

                                    @if ($errors->any())
                                          <ul class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                @endforeach
                                          </ul>
                                    @endif

                                    {!! Form::model($doctor, [
                                        'method' => 'PATCH',
                                        'url' => ['/doctor', $doctor->id],
                                        'class' => 'form-horizontal',
                                        'files' => true
                                    ]) !!}

                                    @include ('doctor.form', ['submitButtonText' => 'Update'])

                                    {!! Form::close() !!}

                              </div>
                        </div>
                  </div>
            </div>
      </div>
@endsection
