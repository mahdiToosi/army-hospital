<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/slick-1.8.1/slick/slick-theme.css"/>
    <link rel="stylesheet" href="/css/app.css">

        @yield('style')

    <title>Document</title>

</head>

<body>

    <div id="app">

        @if(\Request::route()->getname() === 'login')
            <slick ref="slick" :options="slickOptions">
                <div>
                    <img class="mahdi slickcss"
                            src="{{asset('img/slick/dr.jpg')}}" alt="">
                </div>
            </slick>
        @endif
        @if(\Request::route()->getname() === 'Oprator.Home')
            <slick ref="slick" :options="slickOptions">

                <?php
                $randomNums = [];
                for($i= 0; $i<40; $i++) {
                    $randomNum = rand(1, 154);
                    array_push($randomNums , $randomNum);
                }
                ?>
                @foreach($randomNums as $randomNum)
                    <div >
                        <img  class="slickcss" src="{{asset("img/slick/$randomNum.jpg")}}"  alt="">
                    </div>
                @endforeach
            </slick>
        @endif

        @yield('content')

    </div>

    <script src="{{asset('js/persian-date.js')}}" ></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    @yield('js')
    <script src="{{asset('js/app.js')}}"></script>

</body>
</html>
