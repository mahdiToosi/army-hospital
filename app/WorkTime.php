<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkTime extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'work_times';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['dr_id', 'start', 'end', 'visit_time', 'day_id' , 'day'];

      public function doctor()
      {
            return $this->belongsTo('App\Doctor','dr_id' );
      }
}
