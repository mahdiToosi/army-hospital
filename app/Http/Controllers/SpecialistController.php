<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Specialist;
use App\Doctor;
use Illuminate\Http\Request;

class SpecialistController extends Controller
{
      public function __construct()
      {
            $this->middleware('admin')->except('getSpecialitys');
      }

      public function getSpecialitys()
      {
            $Specialitys = Specialist::get()->all();
            return $Specialitys;
      }
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\View\View
       */
      public function index(Request $request)
      {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                  $specialist = Specialist::where('speciality', 'LIKE', "%$keyword%")
                        ->latest()->paginate($perPage);
            } else {
                  $specialist = Specialist::latest()->paginate($perPage);
            }

            return view('specialist.index', compact('specialist'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\View\View
       */
      public function create()
      {
            return view('specialist.create');
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function store(Request $request)
      {

            $requestData = $request->all();
            $validatedData = $request->validate([
                  'speciality' => 'required|min:4|max:255|unique:specialists',
            ]);

            Specialist::create($requestData);

            return redirect('specialist')->with('flash_message', 'Specialist added!');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function destroy($id)
      {
            Doctor::where('specialist_id', $id)->delete();
            
            Specialist::destroy($id);

            return redirect('specialist');
      }
}
