<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Off;
use App\WorkTime;
use Carbon\Carbon;

use App\Patient;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock;

class PatientController extends Controller
{
      public function getPatientWithDate($date , $docId)
      {

            if ($this->DrIsOn($date , $docId)){
                  $patients = Patient::whereDate('date_time' ,$date)
                        ->where('dr_id', $docId)
                        ->get();
                  $list = [];
                  if ((bool)$patients){
                        foreach ($patients as $patient){
                              $patientsData = [
                                    'id'                          => $patient->id,
                                    'name'                    => $patient->name ,
                                    'timeDate'              => $patient->date_time,
                                    'phone_number'    => $patient->phone_number,
                                    'doctor_id'             => $patient->doctor->id,
                                    'done'             => $patient->done,
                              ];
                              array_push($list , $patientsData);
                        }
                  }
                  $thisDateYearInt = (int)substr($date,0,4);
                  $thisDateMonthInt = (int)substr($date,5,2);
                  $thisDateDayInt = (int)substr($date,8,2);
                  $thisDateWithCarbon = Carbon::create($thisDateYearInt, $thisDateMonthInt, $thisDateDayInt);

                  $thisDateDayOfWeek = $thisDateWithCarbon->dayOfWeek;
                  $workTime = WorkTime::where('day_id' , $thisDateDayOfWeek)
                                                      ->where('dr_id' , $docId )->first();

                  if (!$workTime){
//                        echo 'toosi';
                        return null;
                  }

                  $start = $workTime->start;
                  $startHourInt = (int)substr($start , 0, 2);
                  $startMinuteInt = (int)substr($start, 3, 2);
                  $startTimeWithCarbon = Carbon::create($thisDateYearInt, $thisDateMonthInt,
                        $thisDateDayInt ,$startHourInt, $startMinuteInt );
                  $end = $workTime->end;

                  $endHourInt = (int)substr($end , 0, 2);
                  $endMinuteInt = (int)substr($end , 3, 2);
                  $endTimeWithCarbon = Carbon::create($thisDateYearInt, $thisDateMonthInt,
                        $thisDateDayInt ,$endHourInt, $endMinuteInt )->toDateTimeString();

                  $diffStartEnd = $startTimeWithCarbon->diffInMinutes($endTimeWithCarbon);
                  $visitTime = $workTime->visit_time;
                  $howManyVisit = $diffStartEnd / $visitTime;
//                  dd($startTimeWithCarbon);
                  for($i  = 0; $i < $howManyVisit; $i++ ) {
                        if ($i == 0) {
                              $timeDate = $startTimeWithCarbon->toDateTimeString();
                        } else {
                              $timeDate = $startTimeWithCarbon->addMinutes($visitTime)->toDateTimeString();
                        }

                        foreach ($list as $k => $v){
                              if($v['timeDate']  == $timeDate){
                                    continue 2;
                              }
                        }
                        $setTimeArray = [
                              'timeDate'      => $timeDate ,
                        ];
                        array_push($list , $setTimeArray);
                  }

                  $sortedList = array_values(array_sort($list, function ($value) {
                        return $value['timeDate'];
                  }));
                  return $sortedList;

            }else{
//                  echo 'dr is off';
                  return null;
            }
      }


      public function DrIsOn($dateTime , $docId)
      {
            $date = substr($dateTime,0,10);
            $docIsOn = Off::whereDate('date' ,$date)
                                    ->where('dr_id', $docId)
                                    ->first();
            if((bool)$docIsOn){
                  return false;
            }else{
                  return true;
            }
      }

      public function patientDone($id)
      {
            $patient = Patient::find($id);
            $patient->done = 1 ;
            $patient->save();
      }




      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\View\View
       */
      public function index(Request $request)
      {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                  $patient = Patient::where('name', 'LIKE', "%$keyword%")
                        ->orWhere('phone_number', 'LIKE', "%$keyword%")
                        ->orWhere('date_time', 'LIKE', "%$keyword%")
                        ->orWhere('dr_id', 'LIKE', "%$keyword%")
                        ->latest()->paginate($perPage);
            } else {
                  $patient = Patient::latest()->paginate($perPage);
            }

            return view('patient.index', compact('patient'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\View\View
       */
      public function create()
      {
            return view('patient.create');
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function store(Request $request)
      {
            $requestData = $request->all();

            Patient::create($requestData);

            return redirect('patient')->with('flash_message', 'Patient added!');
      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function show($id)
      {
            $patient = Patient::findOrFail($id);

            return view('patient.show', compact('patient'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function edit($id)
      {
            $patient = Patient::findOrFail($id);

            return view('patient.edit', compact('patient'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function update(Request $request, $id)
      {

            $requestData = $request->all();

            $patient = Patient::findOrFail($id);
            $patient->update($requestData);

            return redirect('patient')->with('flash_message', 'Patient updated!');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function destroy($id)
      {
            Patient::destroy($id);

            return redirect('patient')->with('flash_message', 'Patient deleted!');
      }
}
