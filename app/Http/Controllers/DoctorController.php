<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Doctor;
use App\Specialist;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
      public function __construct()
      {
            $this->middleware('admin')->except('getDoctors' ,'getDoctorsWithSpecialityId');
      }

      public function getDoctors()
      {
            $Doctors = Doctor::get()->all();
            return $Doctors;
      }

      public $days = [
            'شنبه' , 'یکشنبه' , 'دوشنبه' , 'سه شنبه' , 'چهارشنبه' , 'پنجشنبه' , 'جمعه'
      ];


      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\View\View
       */
      public function index(Request $request)
      {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                  $doctor = Doctor::where('name', 'LIKE', "%$keyword%")
                                             ->latest()->paginate($perPage);
            } else {
                  $doctor = Doctor::latest()->paginate($perPage);
            }
            return view('doctor.index', compact('doctor'));
      }

      public function getDoctorsWithSpecialityId($specialityId)
      {
            $speciality = Doctor::where('specialist_id',$specialityId)->get()->all();
            return $speciality;
      }
      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\View\View
       */
      public function create()
      {
            $specialitys = Specialist::all();
            return view('doctor.create' , compact('specialitys' , 'days'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function store(Request $request)
      {
            $requestData = $request->all();
            
            $validatedData = $request->validate([
                  'name' => 'required|min:4|max:255|unique:doctors',
                  'phone_number' => 'required|min:7|max:14|unique:doctors',
            ]);

            Doctor::create($requestData);
            return redirect(route('work-time.index'))->with('flash_message', 'Doctor added!');
      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function show($id)
      {
            $doctor = Doctor::findOrFail($id);

            return view('doctor.show', compact('doctor'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function edit($id)
      {
            $doctor = Doctor::findOrFail($id);
            $specialitys = Specialist::all();
            return view('doctor.edit', compact('doctor' , 'specialitys'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function update(Request $request, $id)
      {

            $requestData = $request->all();

            $doctor = Doctor::findOrFail($id);
            $doctor->update($requestData);

            return redirect('doctor')->with('flash_message', 'Doctor updated!');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function destroy($id)
      {
            Doctor::destroy($id);

            return redirect('doctor')->with('flash_message', 'Doctor deleted!');
      }
}
