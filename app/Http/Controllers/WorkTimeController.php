<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\WorkTime;
use Illuminate\Http\Request;

class WorkTimeController extends Controller
{
      public function __construct()
      {
            $this->middleware('admin');
      }
      public $days = [
            'یکشنبه' , 'دوشنبه' , 'سه شنبه' , 'چهارشنبه' , 'پنجشنبه' , 'جمعه' , 'شنبه'
      ];
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\View\View
       */
      public function index(Request $request)
      {
            $perPage = 25;
            $worktime = WorkTime::latest()->paginate($perPage);
            $days = $this->days;
            $docs= Doctor::get()->all();
//            foreach($worktime as $item){
//                  print_r($item->doctor->name);
//            }
//            dd($worktime);
            return view('work-time.index', compact('worktime' , 'docs' , 'days'));
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return \Illuminate\View\View
       */
      public function create()
      {
            $days = $this->days;
            $docs= Doctor::get()->all();

            return view('work-time.create' , compact('days' ,'docs'));
      }

      /**
       * Store a newly created resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function store(Request $request)
      {

            $requestData = $request->all();
            $day = $this->days[$request->day_id];
            $requestData['day'] = $day;

            WorkTime::create($requestData);

            return redirect('work-time')->with('flash_message', 'WorkTime added!');
      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function show($id)
      {
            $worktime = WorkTime::where('dr_id', '=' , $id)->get();
            return view('work-time.show', compact('worktime'));
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       *
       * @return \Illuminate\View\View
       */
      public function edit($id)
      {
            $worktime = WorkTime::findOrFail($id);
            $days = $this->days;
            return view('work-time.edit', compact('worktime', 'days'));
      }

      /**
       * Update the specified resource in storage.
       *
       * @param \Illuminate\Http\Request $request
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function update(Request $request, $id)
      {

            $requestData = $request->all();

            $worktime = WorkTime::findOrFail($id);
            $worktime->update($requestData);

            return redirect('work-time')->with('flash_message', 'WorkTime updated!');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       *
       * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
       */
      public function destroy($id)
      {
            WorkTime::destroy($id);

            return redirect('work-time')->with('flash_message', 'WorkTime deleted!');
      }
}
