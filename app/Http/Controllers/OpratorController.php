<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Oprator;
use App\WorkTime;
use Carbon\Carbon;

class OpratorController extends Controller
{
      public function __construct()
      {
            $this->middleware('oprator');
      }

      public function showHome()
      {
            return view('oprator.home');
      }

      public function getDoctors()
      {
            $dayOfWeek = Carbon::now()->dayOfWeek;
            $thisDayDoctors = WorkTime::where('day_id' , $dayOfWeek )->get();
            if(count($thisDayDoctors) === 0){
                  return 0;
            }
            $list = [];
            foreach ($thisDayDoctors as $doctor){
                  foreach ($list as $k => $v){
                        if ($v['id'] === $doctor->doctor->id){
                              continue 2;
                        }
                  }
                  $array = [
                        'id'                    => $doctor->doctor->id,
                        'name'               => $doctor->doctor->name ,
                        'speciality_id'      => $doctor->doctor->specialist_id,
                        'phone_number' => $doctor->doctor->phone_number,
                        'speciality'          => $doctor->doctor->speciality->speciality,
                  ];
                  array_push($list , $array);
            }
            return $list;
      }

      public function delete($id)
      {
            Oprator::destroy($id);
            return redirect(route('oprator.register'));
      }
}
