<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Off extends Model
{
      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'offs';
      
      /**
       * The database primary key value.
       *
       * @var string
       */
      protected $primaryKey = 'id';
      
      /**
       * Attributes that should be mass-assignable.
       *
       * @var array
       */
      protected $fillable = ['date', 'dr_id' , 'jalaliDate'];

      public function doctor()
      {
            return $this->belongsTo('App\Doctor','dr_id' );
      }
}
