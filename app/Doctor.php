<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'doctors';

      /**
       * The database primary key value.
       *
       * @var string
       */
      protected $primaryKey = 'id';

      /**
       * Attributes that should be mass-assignable.
       *
       * @var array
       */
      protected $fillable = ['name', 'phone_number', 'specialist_id'];

      public function speciality()
      {
            return $this->belongsTo('App\Specialist','specialist_id' );
      }

      public function workTime()
      {
            return $this->hasMany('App\WorkTime','dr_id' );
      }
      public function off()
      {
            return $this->hasMany('App\Off','dr_id' );
      }

}
