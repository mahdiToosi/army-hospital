<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialist extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'specialists';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['speciality'];


    public function Doctors()
    {
        return $this->belongsTo(Doctor::class);
    }
    
}
